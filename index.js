const express = require("express");
const app = express();
const fs = require('fs');
const axios = require("axios");
const formidable = require('formidable');
app.use(express.json());

app.get('/admin', async function(req, res) {
res.setHeader("Access-Control-Allow-Origin","*");
var config = JSON.parse(Buffer.from((await axios('https://api.github.com/repos/theabbie/theabbie.github.io/contents/config.json')).data.content,"base64").toString());
axios('https://api.github.com/repos/theabbie/'+config.repo+'/contents/'+config.dir+'/'+req.query.file+'?access_token=374fb8fa5f6f82cdad527a8ba0845d76b55e3ccd').then(function(x) {
if(req.query.data || req.query.append) {
axios({
  method: 'put',
  url: 'https://api.github.com/repos/theabbie/'+config.repo+'/contents/'+config.dir+'/'+req.query.file,
  data: {
  "message": "my commit message",
  "committer": {
    "name": "abbie",
    "email": "abhishek7gg7@gmail.com"
  },
  "sha": x.data.sha,
  "content": Buffer.from((req.query.append?Buffer.from(x.data.content, 'base64').toString():"")+(req.query.append || req.query.data)).toString('base64')
},
headers: {
 "Content-Type" : "application/vnd.github.v3+json",
 "Authorization" : "token 374fb8fa5f6f82cdad5"+"27a8ba0845d76b55e3ccd"
}
}).then(function(m) {
res.end();
})
}
else {
res.end()
}
}).catch(function(y) {
if (req.query.data || req.query.append) {
axios({
  method: 'put',
  url: 'https://api.github.com/repos/theabbie/'+config.repo+'/contents/'+config.dir+'/'+req.query.file,
  data: {
  "message": "my commit message",
  "committer": {
    "name": "abbie",
    "email": "abhishek7gg7@gmail.com"
  },
  "content": Buffer.from(req.query.data || req.query.append).toString('base64')
},
headers: {
 "Content-Type" : "application/vnd.github.v3+json",
 "Authorization" : "token 374fb8fa5f6f82cdad5"+"27a8ba0845d76b55e3ccd"
}
}).then(function(n) {
res.end();
})
}
else {
res.end()
}
})
});

app.get('/*', function(req, res) {
res.setHeader("Access-Control-Allow-Origin","*");
axios('https://api.github.com/repos/theabbie/data/contents/db'+req.url.split("?")[0]+'?access_token=374fb8fa5f6f82cdad527a8ba0845d76b55e3ccd').then(function(x) {
if(req.query.data || req.query.append) {
axios({
  method: 'put',
  url: 'https://api.github.com/repos/theabbie/data/contents/db'+req.url,
  data: {
  "message": "my commit message",
  "committer": {
    "name": "abbie",
    "email": "abhishek7gg7@gmail.com"
  },
  "sha": x.data.sha,
  "content": Buffer.from((req.query.append?Buffer.from(x.data.content, 'base64').toString():"")+(req.query.append || req.query.data)).toString('base64')
},
headers: {
 "Content-Type" : "application/vnd.github.v3+json",
 "Authorization" : "token 374fb8fa5f6f82cdad5"+"27a8ba0845d76b55e3ccd"
}
}).then(function(m) {
res.end();
})
}
else {
res.send(Buffer.from(x.data.content, 'base64'))
}
}).catch(function(y) {
if (req.query.data || req.query.append) {
axios({
  method: 'put',
  url: 'https://api.github.com/repos/theabbie/data/contents/db'+req.url,
  data: {
  "message": "my commit message",
  "committer": {
    "name": "abbie",
    "email": "abhishek7gg7@gmail.com"
  },
  "content": Buffer.from(req.query.data || req.query.append).toString('base64')
},
headers: {
 "Content-Type" : "application/vnd.github.v3+json",
 "Authorization" : "token 374fb8fa5f6f82cdad5"+"27a8ba0845d76b55e3ccd"
}
}).then(function(n) {
res.end();
})
}
else {
res.end()
}
})
});

app.post("/file", function(req,res) {
var form = new formidable.IncomingForm();
form.parse(req, function (err, fields, files) {
var data = req.body.b64 || fs.readFileSync(files.file.path, { encoding: 'base64' })
axios({
  method: 'put',
  url: 'https://api.github.com/repos/theabbie/data/contents/db/'+files.file.name,
  data: {
  "message": "my commit message",
  "committer": {
    "name": "abbie",
    "email": "abhishek7gg7@gmail.com"
  },
  "content": data
},
headers: {
 "Content-Type" : "application/vnd.github.v3+json",
 "Authorization" : "token 374fb8fa5f6f82cdad5"+"27a8ba0845d76b55e3ccd"
}
}).then(function(n) {res.redirect(301, req.headers.referer)})
});
});

app.post("/admin",async function(req,res) {
var form = new formidable.IncomingForm();
form.parse(req,async function (err, fields, files) {
var config = JSON.parse(Buffer.from((await axios('https://api.github.com/repos/theabbie/theabbie.github.io/contents/config.json')).data.content,"base64").toString());
var data = req.body.b64 || fs.readFileSync(files.file.path, { encoding: 'base64' })
axios({
  method: 'put',
  url: 'https://api.github.com/repos/theabbie/'+config.repo+'/contents/'+config.uploaddir+'/'+(req.body.name || files.file.name),
  data: {
  "message": "my commit message",
  "committer": {
    "name": "abbie",
    "email": "abhishek7gg7@gmail.com"
  },
  "content": data
},
headers: {
 "Content-Type" : "application/vnd.github.v3+json",
 "Authorization" : "token 374fb8fa5f6f82cdad5"+"27a8ba0845d76b55e3ccd"
}
}).then(function(n) {res.redirect(301, req.headers.referer)})
});
});

app.post('/*', function(req, res) {
res.setHeader("Access-Control-Allow-Origin","*");
axios('https://api.github.com/repos/theabbie/data/contents/db'+req.url.split("?")[0]+'?access_token=374fb8fa5f6f82cdad527a8ba0845d76b55e3ccd').then(function(x) {
if(req.body.data || req.body.append) {
axios({
  method: 'put',
  url: 'https://api.github.com/repos/theabbie/data/contents/db'+req.url,
  data: {
  "message": "my commit message",
  "committer": {
    "name": "abbie",
    "email": "abhishek7gg7@gmail.com"
  },
  "sha": x.data.sha,
  "content": Buffer.from((req.body.append?Buffer.from(x.data.content, 'base64').toString():"")+(req.body.append || req.body.data)).toString('base64')
},
headers: {
 "Content-Type" : "application/vnd.github.v3+json",
 "Authorization" : "token 374fb8fa5f6f82cdad5"+"27a8ba0845d76b55e3ccd"
}
}).then(function(m) {
res.end();
})
}
else {
res.send(Buffer.from(x.data.content, 'base64'))
}
}).catch(function(y) {
if (req.body.data || req.body.append) {
axios({
  method: 'put',
  url: 'https://api.github.com/repos/theabbie/data/contents/db'+req.url,
  data: {
  "message": "my commit message",
  "committer": {
    "name": "abbie",
    "email": "abhishek7gg7@gmail.com"
  },
  "content": Buffer.from(req.body.data || req.body.append).toString('base64')
},
headers: {
 "Content-Type" : "application/vnd.github.v3+json",
 "Authorization" : "token 374fb8fa5f6f82cdad5"+"27a8ba0845d76b55e3ccd"
}
}).then(function(n) {
res.end()
})
}
else {
res.end()
}
})
});


app.listen(process.env.PORT);
